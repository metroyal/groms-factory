<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail from user at gromsfactory.com</title>
</head>
<body>
    <h2>Sender Name: {!! $dataAdmin['name'] !!}</h2>
    <h2>Email: {!! $dataAdmin['email'] !!}</h2>
    </br>
    <h2>Phone: {!! $dataAdmin['phone'] !!}</h2>
    </br>
    <p>Message: {!! $dataAdmin['content'] !!}</p>
</body>
</html>