<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
  <link rel="icon" type="image/x-icon" href="./assets/img/favicon.ico">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Groms Factory
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,800|Material+Icons" />
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"> -->
  <!-- CSS Files -->
  <link href="./assets/css/material-kit.css?v=2.0.6" rel="stylesheet" />
  <link href="./assets/demo/demo.css" rel="stylesheet" />
  <link href="{{URL::asset('css/app.css')}}" rel="stylesheet" />
  
  <style>
    .iconBig {
      font-size: 100px;
    }

    .iconNormal {
      font-size: 18px !important;
    }

    /* Email Loading Styling */

    .formButton {
		position: relative;
		height: 70px;
    }
    #finish {
      transition: all 300ms easy!important;
      -webkit-transition: all 300ms !important;
      position: absolute;
      opacity: 0;
      left: 85px;
      top: -18px;
    }

    #transition {
      transition: all 300ms !important;
      -webkit-transition: all 300ms !important;
    }
    .move-icon{
          transform: translate(-32px, 5px)!important;
    }
    .loading {
      display: block !important;
      opacity: 0;
      left: 105px;
      position: absolute;
      /* display: none; */
    }
    .reveal{
      opacity: 1 !important;
    }
    .text-vanish{
      opacity: 0;
      display: none !important;
    }
  </style>
  <!-- GA -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-80655368-3"></script>
  <script type="text/javascript">
      if (document.location.hostname == "gromsfactory.com" || document.location.hostname == "www.gromsfactory.com") {
          // Google Tag Manager
          // Not Using GTM //
          // End Google Tag Manager

          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-80655368-3');
          console.log('host --> ', document.location.hostname);
      } else {
          console.log('ini adalah localhost --> ', document.location.hostname);
      }
  </script>
</head>
<!-- <body> -->
<body class="index-page sidebar-collapse">
  <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
    <div class="container text-primary">
      <div class="navbar-translate">
        <a class="navbar-brand" href="#">
          <img src="/assets/img/logo_gromsSmall.png" height="50">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" onclick="scrollToInfo()">
              <i class="fas fa-info-circle"></i> About this Event
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" onclick="scrollToCategory()">
              <i class="material-icons">layers</i> Categories
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" onclick="scrollToPartners()">
              <i class="fas fa-hands-helping"></i> Partners
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" onclick="scrollToMail()">
              <i class="far fa-envelope-open"></i> Become a Sponsor
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link iconNormal" rel="tooltip" title="" data-placement="bottom" href="https://www.instagram.com/explore/tags/gromsfactory/" target="_blank" data-original-title="Follow Hashtag on Instagram">
              <i class="fab fa-instagram"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="page-header header-filter white-filter clear-filter" data-parallax="true" style="background-image: url('./assets/img/gromsfactoryBg.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand">
            <img src="/assets/img/logo_gromsBig.png">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="main main-raised">
    <div class="container">
      <div class="section section-contacts">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <h2 class="text-center title">Registration</h2>
            <div class="icon iconBig icon-primary text-center">
              <!-- <i class="far fa-edit"></i> -->
              <a href="http://www.hyperscore.net/gromsfactory" target="_blank">
                <img src="../assets/img/hst_logo.png" alt="Thumbnail Image" width="80" class="img-fluid">
              </a>
            </div>
            <h4 class="text-center description">The registration is available through <a href="http://www.hyperscore.net/gromsfactory" target="_blank">Hyper Score Technology</a>.</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 ml-auto mr-auto text-center">
            <a href="http://www.hyperscore.net/gromsfactory" target="_blank" class="btn btn-primary btn-raised">
              Register Here
            </a>
          </div>
        </div>
      </div>
      <div class="section text-center infoSection">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <h2 class="title">About Groms Factory</h2>
            <h5 class="description">Groms factory is a series of contest that help raise up the young guns skater to compete on top level at young age. So the top limit of age to involve in this contest is up to 15 years old. This will be a series of event in 2020, and would take place in 3 skateparks around Jakarta and Tangerang.</h5>
          </div>
        </div>
        <div class="features">
          <div class="row">
            <div class="col-md-4">
              <div class="card card-plain">
                <div class="icon iconBig">
                  <img src="{{URL::asset('assets/img/brands/brand_elbricks.png')}}">
                </div>
                <h4 class="info-title">1st Series</br>Elbricks Skatepark</h4>
                <p>East Jakarta</p>
                <p>8 March 2020</p>
                <div class="card-footer justify-content-center">
                  <a href="" data-target="#mapsElbrick" data-toggle="modal" class="btn btn-link mapsIconMobileSize"><i class="fas fa-map"></i> Location Maps</a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-plain">
                <div class="icon iconBig">
                  <i class="fas fa-map-marker-alt"></i>
                </div>
                <h4 class="info-title">2nd Series</br>To Be Announce</h4>
                <p>-</p>
                <p>28 June 2020</p>
                <div class="card-footer justify-content-center">
                  <a href="" data-target="#mapsBsd" data-toggle="modal" class="btn btn-link mapsIconMobileSize"><i class="fas fa-map"></i> Location Maps</a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-plain">
                <div class="icon iconBig">
                  <i class="fas fa-map-marker-alt"></i>
                </div>
                <h4 class="info-title">3rd Series</br>To Be Announce</h4>
                <p>-</p>
                <p>9 August 2020</p>
                <div class="card-footer justify-content-center">
                  <a href="" data-target="#mapsPasarrebo" data-toggle="modal" class="btn btn-link mapsIconMobileSize"><i class="fas fa-map"></i> Location Maps</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section text-center categorySection">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <h2 class="title">Categories</h2>
            <h5 class="description">Groms factory will be devided onto 3 categories of Under 15.</h5>
          </div>
        </div>
        <div class="features">
          <div class="row">
            <div class="col-md-4">
              <div class="info">
                <div class="icon iconBig icon-info">
                  <i class="fas fa-male"></i>
                </div>
                <h4 class="info-title">Boys Open</h4>
                <p>For boys age up to 15 years old experienced in skateboard contest.</p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="info">
                <div class="icon iconBig icon-success">
                  <i class="fas fa-female"></i>
                </div>
                <h4 class="info-title">Girls Open</h4>
                <p>For girls age up to 15 years old experienced in skateboard contest.</p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="info">
                <div class="icon iconBig icon-danger">
                  <i class="fas fa-child"></i>
                </div>
                <h4 class="info-title">Beginners Boys & Girls</h4>
                <p>For boys and girls age up to 10 years old never/less experienced in skateboard contest before.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section text-center partnersSection">
        <h2 class="title">Partners</h2>
        <div class="team">
          <div class="row">
            <div class="col-md-4">
              <div class="team-player">
                <div class="card card-plain">
                  <div class="col-md-6 ml-auto mr-auto">
                    <img src="../assets/img/logo_labskate.png" alt="Thumbnail Image" class="img-fluid">
                  </div>
                  <h4 class="card-title">LAB Skate
                    <br>
                    <small class="card-description text-muted">Event Organizer</small>
                  </h4>
                  <div class="card-body">
                    <p class="card-description">More than 10 years of Experience create action sports event, some great achievements and amazing experience has been made a lot here.</p>
                  </div>
                  <div class="card-footer justify-content-center">
                    <a href="https://www.instagram.com/labskate.id/" target="_blank" class="btn btn-link iconMobileSize btn-just-icon"><i class="fab fa-instagram"></i></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="team-player">
                <div class="card card-plain">
                  <div class="col-md-6 ml-auto mr-auto">
                    <img src="../assets/img/hst_logo.png" alt="Thumbnail Image" class="img-fluid">
                  </div>
                  <h4 class="card-title">Hyper Score Technology
                    <br>
                    <small class="card-description text-muted">Event Manager App & Live Scoring System</small>
                  </h4>
                  <div class="card-body">
                    <p class="card-description">HST (Hyper Score Technology) is an action sports Contest Management, scoring app, and administration in one complete system. This services will define a contest can become well managed, easy, fun, and professional.</p>
                  </div>
                  <div class="card-footer justify-content-center">
                    <a href="http://www.hyperscore.net" target="_blank" class="btn btn-link iconMobileSize btn-just-icon"><i class="fas fa-globe"></i></a>
                    <a href="https://www.instagram.com/hyperscoretechnology/" target="_blank" class="btn btn-link iconMobileSize btn-just-icon"><i class="fab fa-instagram"></i></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="team-player">
                <div class="card card-plain">
                  <div class="col-md-6 ml-auto mr-auto">
                    <img src="../assets/img/supa_logo.png" alt="Thumbnail Image" class="img-fluid">
                  </div>
                  <h4 class="card-title">Supa Skateboarding
                    <br>
                    <small class="card-description text-muted">Skateboarding Media</small>
                  </h4>
                  <div class="card-body">
                    <p class="card-description">One of Skateboarding Media in Indonesia, a partner to become a center of information for this series of event.</p>
                  </div>
                  <div class="card-footer justify-content-center">
                    <a href="https://www.instagram.com/supaskateboarding/" target="_blank" class="btn btn-link iconMobileSize btn-just-icon"><i class="fab fa-instagram"></i></a>
                    <a href="https://www.facebook.com/SupaSkateboarding/" target="_blank" class="btn btn-link iconMobileSize btn-just-icon"><i class="fab fa-facebook-square"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <h2 class="title">Sponsors</h2>
          <div class="row outerContainer">
            <div class="borderBottom">
              <div class="brandGrid">
                <div class="borderGrid">
                  <img src="{{URL::asset('assets/img/brands/brand_scratch.png')}}">
                </div>
              </div>
            </div>
            <div class="borderBottom">
              <div class="brandGrid">
                <div class="borderGrid">
                  <img src="{{URL::asset('assets/img/brands/brand_44.png')}}">
                </div>
              </div>
            </div>
            <div class="borderBottom">
              <div class="brandGrid">
                <div class="borderGrid">
                  <img src="{{URL::asset('assets/img/brands/brand_metroyal.png')}}">
                </div>
              </div>
            </div>
            <div class="borderBottom">
              <div class="brandGrid">
                <div class="borderGrid">
                  <img src="{{URL::asset('assets/img/brands/brand_etaks.png')}}">
                </div>
              </div>
            </div>
            <div class="borderBottom">
              <div class="brandGrid">
                <div class="borderGrid">
                  <img src="{{URL::asset('assets/img/brands/brand_fyc.png')}}">
                </div>
              </div>
            </div>
            <div class="borderBottom">
              <div class="brandGrid">
                <div class="borderGrid">
                  <img src="{{URL::asset('assets/img/brands/brand_jakcloth.png')}}">
                </div>
              </div>
            </div>
            <div class="borderBottom">
              <div class="brandGrid">
                <div class="borderGrid">
                  <img src="{{URL::asset('assets/img/brands/brand_urbain.png')}}">
                </div>
              </div>
            </div>
            <div class="borderBottom">
              <div class="brandGrid">
                <div class="borderGrid">
                  <img src="{{URL::asset('assets/img/brands/brand_triple8.png')}}">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section section-contacts">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <h2 class="text-center title">Video Trailer</h2>
            <div class="youtube-embed-wrapper" style="position:relative;padding-bottom:56.25%;padding-top:30px;height:0;overflow:hidden"><iframe allow=";" width="640" height="360" src="https://www.youtube.com/embed/yOX78-iTmY8" style="position:absolute;top:0;left:0;width:100%;height:100%" frameborder="0" allowfullscreen=""></iframe></div>
          </div>
        </div>
      </div>
      <div class="section section-contacts mailSection">
      {{ csrf_field() }}
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <h2 class="text-center title">Become a Sponsor</h2>
            <h4 class="text-center description">Let&apos;s talk to know more about the event and find out how we can work together</h4>
            <form class="contact-form">
              <div class="row">
                <div class="col-md-4 col-sm-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Your Name</label>
                    <input type="text" name="nameInput" class="form-control" required>
                  </div>
                </div>
                <div class="col-md-4 col-sm-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Your Email</label>
                    <input type="email" name="emailInput" class="form-control" required>
                  </div>
                </div>
                <div class="col-md-4 col-sm-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Phone Number / WhatsApp</label>
                    <input id="phone" type="text" name="phoneInput" class="form-control" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="contentMessage" class="bmd-label-floating">Your Message</label>
                <textarea type="email" name="messageInput" class="form-control" rows="4" id="contentMessage" required></textarea>
              </div>
              <div class="row">
                <div class="col-md-4 ml-auto mr-auto text-center">
                  <i id="transition" class="fas fa-circle-notch fa-spin loading" style="font-size: 40px; color: #35e3a0; text-align: center;"></i>
                  <div id="finish" style="width: 75px;"></div>
                  <div id="submit_btn" class="btn btn-primary btn-raised sendEmail">
                    Send Message
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Classic Modal -->
  <div class="modal fade" id="mapsElbrick" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Maps</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body google-maps">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.3224117334466!2d106.88468981458126!3d-6.221148262662453!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3368e924d31%3A0x714d89e461a0e37c!2sElbricks%20Skatepark!5e0!3m2!1sen!2sid!4v1581851305831!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-link">Nice Button</button> -->
          <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="mapsPasarrebo" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Maps</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body">
          <h4 class="info-title">Coming Soon</h4>
          <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.6672334679524!2d106.86251111458209!3d-6.307376063481958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69eda5c4f53a97%3A0xcbdd2aae18a69bc8!2sSkatepark%20Kolong%20Fly%20Over%20Pasar%20Rebo!5e0!3m2!1sen!2sid!4v1581852283160!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe> -->
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-link">Nice Button</button> -->
          <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="mapsBsd" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Maps</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body">
          <h4 class="info-title">Coming Soon</h4>
          <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.6497799672693!2d106.65135771458216!3d-6.3096570635037885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69e4ca1d18cc69%3A0x43a16e7fd9e796e2!2sBSD%20Xtreme%20Park!5e0!3m2!1sen!2sid!4v1581852322122!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe> -->
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-link">Nice Button</button> -->
          <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!--  End Modal -->
  <footer class="footer" data-background-color="black">
    <div class="container">
      <nav class="float-left">
        <ul>
          <li>
            <a href="javascript:void(0)" onclick="scrollToInfo()">
              About this Event
            </a>
          </li>
        </ul>
      </nav>
      <div class="copyright float-right">
        &copy;
        <script>
          document.write(new Date().getFullYear())
        </script> Groms Factory
      </div>
    </div>
  </footer>
  <!--   Core JS Files   -->
  <script src="./assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  @include('notification')
  <script src="./assets/js/plugins/moment.min.js"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="./assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="./assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
  <script src='{{URL::asset("js/lottie.min.js")}}'></script>
  <script>
    // Check Phone Number
    var inputEl = document.querySelector('#phone');
    var goodKey = '0123456789+ ';
    // console.log(inputEl);

    var checkInputTel = function(e) {
      var key = (typeof e.which == "number") ? e.which : e.keyCode;
      var start = this.selectionStart,
        end = this.selectionEnd;

      var filtered = this.value.split('').filter(filterInput);
      this.value = filtered.join("");

      /* Prevents moving the pointer for a bad character */
      var move = (filterInput(String.fromCharCode(key)) || (key == 0 || key == 8)) ? 0 : 1;
      this.setSelectionRange(start - move, end - move);
    }

    var filterInput = function(val) {
      return (goodKey.indexOf(val) > -1);
    }

    inputEl.addEventListener('input', checkInputTel);

    var animation = bodymovin.loadAnimation({
      container: document.getElementById('finish'),
      renderer: 'svg',
      speed: 1.5,
      loop: false,
      autoplay: false,
      path: '{{URL::asset("assets/img/lf30_editor_P7N6G6.json")}}'
    });
    $('.sendEmail').click(function(){
      $(".loading").addClass("reveal");
      $("#submit_btn").addClass("text-vanish");
      var isPost = 0;
      if (isPost == 0) {
        isPost = 1;
        token = $('input[name="_token"]').val();
        name = $("[name='nameInput']").val();
        phone = $("[name='phoneInput']").val();
        email = $("[name='emailInput']").val();
        message = $("[name='messageInput']").val();
        $.ajax({
          method:"GET",
          data:{token:token,nameInput:name,phoneInput:phone,emailInput:email,messageInput:message},
          url:"{{ url('/contactus') }}",
          success: function(response) {
            console.log('response:', response);
            $(".loading").removeClass("reveal");
            $("#finish").addClass("reveal");
            setTimeout(function(){ lottie.play(); }, 100);
            setTimeout(function(){ location.reload(); }, 3000);
          }
        });
      } else {
        alert('Wait! Send email is on process...');
      }
    });
  
    $(document).ready(function() {
      //init DateTimePickers
      materialKit.initFormExtendedDatetimepickers();

      // Sliders Init
      materialKit.initSliders();
    });


    function scrollToDownload() {
      if ($('.section-download').length != 0) {
        $("html, body").animate({
          scrollTop: $('.section-download').offset().top
        }, 1000);
      }
    }

    function scrollToInfo() {
      if ($('.infoSection').length != 0) {
        $("html, body").animate({
          scrollTop: $('.infoSection').offset().top
        }, 1000);
      }
    }

    function scrollToCategory() {
      if ($('.categorySection').length != 0) {
        $("html, body").animate({
          scrollTop: $('.categorySection').offset().top
        }, 1000);
      }
    }

    function scrollToPartners() {
      if ($('.partnersSection').length != 0) {
        $("html, body").animate({
          scrollTop: $('.partnersSection').offset().top
        }, 1000);
      }
    }

    function scrollToMail() {
      if ($('.mailSection').length != 0) {
        $("html, body").animate({
          scrollTop: $('.mailSection').offset().top
        }, 1000);
      }
    }

  </script>
<!-- </body></html> -->
</body>
</html>
