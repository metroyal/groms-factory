<script src="https://cdn.jsdelivr.net/npm/bootstrap-notify@3.1.3/bootstrap-notify.js"></script>

<script>
    @if(Session::has('success'))
        $.notify({
            icon: 'fa fa-paw',
            message: "{{ Session::get('success') }}"
        },{
            type: 'success'
        });
        @php
        Session::forget('success');
        @endphp
    @endif


    @if(Session::has('info'))
        $.notify({
            icon: 'fa fa-paw',
            message: "{{ Session::get('info') }}"
        });
        @php
            Session::forget('info');
        @endphp
    @endif


    @if(Session::has('warning'))
        $.notify({
            icon: 'fa fa-paw',
            message: "{{ Session::get('warning') }}"
        },{
            type: 'warning'
        });
        @php
            Session::forget('warning');
        @endphp
    @endif


    @if(Session::has('error'))
        $.notify({
            icon: 'fa fa-paw',
            message: "{{ Session::get('error') }}"
        },{
            type: 'danger'
        });
        @php
            Session::forget('error');
        @endphp
    @endif
</script>