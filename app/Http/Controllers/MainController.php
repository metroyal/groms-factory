<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Rule;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Validator;
use App\Mail\ToClientEmail;
use App\Mail\ToAdminEmail;

use Mail;

class MainController extends Controller
{
    public function sendEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nameInput'=>'required',
            'emailInput'=>'required|email',
            'phoneInput'=>'required',
            'messageInput'=>'required',
        ]);
        if ($validator->fails()) {
            session()->flash('warning', $validator->messages()->first());
        } else {
            try {
                $dataAdmin = [
                    'name' => $request->nameInput,
                    'phone' => $request->phoneInput,
                    'email' => $request->emailInput,
                    'content' => $request->messageInput,
                    'subject'   => 'Mail from '.$request->nameInput.' at gromsfactory.com',
                    'from'      => $request->emailInput,
                    'from_name' => $request->nameInput
                ];
        
                Mail::to('admin@gromsfactory.com', $request->nameInput." - Groms Factory")->send(new ToAdminEmail($dataAdmin));

                $dataSender = [
                    'name' => $request->nameInput,
                    'phone' => $request->phoneInput,
                    'email' => $request->emailInput,
                    'content' => $request->messageInput,
                    'subject'   => 'Thank You for Contacting Groms Factory Team',
                    'from'      => 'no-reply@gromsfactory.com',
                    'from_name' => 'GromsFactory.com',
                ];
        
                Mail::to($request->emailInput, $request->nameInput)->send(new ToClientEmail($dataSender));
                session()->flash('success','Your mail is sent!');
                return response (['status' => true,'errors' => 'message sent']);
            } catch (Exception $e){
                session()->flash('error','Something Wrong, email not sent!');
                return response (['status' => false,'errors' => $e->getMessage()]);
            }
        }
    }
}