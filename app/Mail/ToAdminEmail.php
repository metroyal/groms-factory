<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ToAdminEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */

    public $dataAdmin;
    public function __construct($dataAdmin)
    {
        $this->dataAdmin = $dataAdmin;
    }

    /**
     * Build the message.
     */
    public function build()
    {
        // return $this->view('view.name');
        return $this->from($this->dataAdmin['from'], $this->dataAdmin['from_name'])
            ->subject($this->dataAdmin['subject'])
            ->view('email.mailtoadmin');
    }
}
