<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ToClientEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */

    public $dataSender;
    public function __construct($dataSender)
    {
        $this->dataSender = $dataSender;
    }

    /**
     * Build the message.
     */
    public function build()
    {
        // return $this->view('view.name');
        return $this->from($this->dataSender['from'], $this->dataSender['from_name'])
            ->subject($this->dataSender['subject'])
            ->view('email.mailtosender');
    }
}
